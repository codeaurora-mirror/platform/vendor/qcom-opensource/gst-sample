/*
 * Copyright (c) 2017,2018 The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef __GST_QTI_VIDEO_POOL_H__
#define __GST_QTI_VIDEO_POOL_H__

#include <gst/gst.h>
#include <gst/video/video.h>

G_BEGIN_DECLS

typedef struct _GstQtiVideoBufferPool GstQtiVideoBufferPool;
typedef struct _GstQtiVideoBufferPoolClass GstQtiVideoBufferPoolClass;
typedef struct _GstQtiVideoBufferPoolPrivate GstQtiVideoBufferPoolPrivate;

typedef enum {
  QTI_VIDEO_POOL_BUFFER_FREE = 0,
  QTI_VIDEO_POOL_BUFFER_ALLOC
} GstQtiVideoPoolBufferNotifyFlag;

/**
 * GstQtiVideoPoolAllocNotify:
 * @usr_data: a pointer to usr data for callback
 * @buffer: a #GstBuffer is allocated/freeing
 * @flag: a flag to indicate the buffer is allocated or freed
 *
 * the callback shall hold the object lock of the qtivideopool,
 * so please avoid doing any operations on the pool in it.
 */
typedef void (*GstQtiVideoPoolAllocNotify) (gpointer usr_data, GstBuffer *buffer,
    GstQtiVideoPoolBufferNotifyFlag flag);

#define GST_TYPE_QTI_VIDEO_BUFFER_POOL      (gst_qti_video_buffer_pool_get_type())
#define GST_IS_QTI_VIDEO_BUFFER_POOL(obj)   (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GST_TYPE_QTI_VIDEO_BUFFER_POOL))
#define GST_QTI_VIDEO_BUFFER_POOL_GET_CLASS(obj)       (G_TYPE_INSTANCE_GET_CLASS ((obj), GST_TYPE_QTI_VIDEO_BUFFER_POOL, GstQtiVideoBufferPoolClass))
#define GST_QTI_VIDEO_BUFFER_POOL(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), GST_TYPE_QTI_VIDEO_BUFFER_POOL, GstQtiVideoBufferPool))
#define GST_QTI_VIDEO_BUFFER_POOL_CAST(obj) ((GstQtiVideoBufferPool*)(obj))

struct _GstQtiVideoBufferPool
{
  GstVideoBufferPool videopool;

  GstQtiVideoBufferPoolPrivate *priv;
};

struct _GstQtiVideoBufferPoolClass
{
  GstVideoBufferPoolClass parent_class;
};

GType             gst_qti_video_buffer_pool_get_type      (void);
GstBufferPool *   gst_qti_video_buffer_pool_new           (void);

/**
 * gst_qti_video_pool_propose_allocation:
 * @ppool: (inout) a pointer to pointer of #GstQtiVideoBufferPool that
 *             the caller has. its value can be %NULL, if %NULL, a new
 *             qtivideopool will be created. Also the input pool may be
 *             unreffed and replaced if it doesn't match the caps in
 *             the query or if it's not a qtivideopool, and a new created
 *             qtivideopool will be returned via this param.
 * @query: (inout) a #GstQuery of allocation
 * @pmin: (in) (allow-none) a pointer to the suggested min count for the pool,
 *             it can be %NULL, if %NULL, 2 will be token as default value.
 * @pmax: (in) (allow-none) a pointer to the suggested max count for the pool,
 *             it can be %NULL, if %NULL, 0 will be token as default value.
 * @force_equal: (in) (allow-none) a pointer to indicate if propose_pool to
 *             force configure min and max are equal when decide_allocation
 *             is called by upstream element with the corresponding
 *             decide_allocation function provided by this utils.
 *             if value of pmin and pmax is equal, force_equal will be
 *             considered as TRUE. NOTE, if force_equal is true, the max
 *             number limitation may be removed when specific element is
 *             followed by the element proposes force_equal or upstream
 *             decide_allocation has a max limitation.
 * @in_align: (in) (allow-none) a pointer to #GstVideoAlignment that will
 *            be applied for the buffer allocated from the pool.
 *
 * a optional propose_allocation for downstream element, while it
 * can be implemented by downstream element itself.
 * GST_VIDEO_META_API_TYPE will be added to the query by default,
 * extra meta can be added after this call if needed.
 * use gst_object_unref() to unref the pool after usage.
 *
 * if the query from upstream doesn't need a pool, then this function shall
 * do nothing, and returns TRUE if query and ppool are valid.
 *
 * !!!!!! NOTE:
 * 1, in_align will be ignored if gbm was configured for this utils;
 *
 * Returns: FALSE if any fails occurs otherwise TRUE will be returned. If false
 *          was returned, please check if the *ppool is NULL or not, if not,
 *          use gst_object_unref() to unref the pool after usage.
 */
gboolean          gst_qti_video_pool_propose_allocation (GstBufferPool **ppool,
                      GstQuery *query, guint *pmin, guint *pmax,
                      gboolean *force_equal, GstVideoAlignment *in_align);
/**
 * gst_qti_video_pool_decide_allocation:
 * @pool: (out) (allow-none) a chosen #GstQtiVideoBufferPool. If none, user
 *             can get the pool from the query. Use gst_object_unref() after
 *             usage of the returned pool here.
 * @query: (inout) a #GstQuery of allocation. it may be updated.
 * @pmin: (in) (allow-none) a pointer to the suggested min count for the pool,
 *             it can be %NULL, if %NULL, min proposed by downstream will be
 *             token, otherwise, the maximum of proposed value by downstream
 *             and *pmin will be token.
 * @pmax: (in) (allow-none) a pointer to the suggested max count for the pool,
 *             it can be %NULL, if %NULL, 0 will be token as default value,
 *             otherwise, the minimal of proposed value by downstream and *pmax
 *             unless force_equal is set by downstream.
 * @in_align: (in) (allow-none) a pointer to #GstVideoAlignment that will
 *            be applied for the buffer allocated from the pool.
 *
 * a optional decide_allocation for upstream element, while it
 * can be implemented by upstream element itself.
 *
 * !!!!!! NOTE (for gbm was configured for this utils):
 * 1, in_align will be ignored
 * 2, input non-NULL pool will be unreffered, new qtivideopool will
 * be created and returned via pool parameter
 *
 * Returns: FALSE if any fails occurs otherwise TRUE will be returned
 */
gboolean          gst_qti_video_pool_decide_allocation (GstBufferPool **pool,
                      GstQuery *query, guint *pmin, guint *pmax,
                      GstVideoAlignment *in_align);
/**
 * gst_qti_video_pool_pre_handle:
 * @pool: a #GstQtiVideoBufferPool of downstream hold
 * @buffer: a #GstBuffer waiting for processing
 *
 * a optional pre_handle for downstream element, while it
 * can be implemented by downstream element itself.
 * the function will check if the buffer is from the pool or the memory
 * of the buffer is from the pool.
 *
 * Returns: TRUE: if the buffer is from the pool (should be qtivideopool)
 *                or the memory of the buffer is from the pool if it
 *                isn't from a pool.
 *          FALSE: for any other conditions. e.g. the pool is %NULL or
 *                the provided pool is not a qtivideopool etc.
 */
gboolean          gst_qti_video_pool_pre_handle (GstBufferPool *pool,
                      GstBuffer *buffer);

/**
 * gst_qti_video_pool_set_allocation_notify:
 * @pool: a #GstQtiVideoBufferPool
 * @usr_data: a pointer to a parameter of callback
 * @notify: a #GstQtiVideoPoolAllocNotify for buffer allocation
 *
 * register a callback to be notified when a gstbuffer of the pool
 * was allocated or freed.
 */
void              gst_qti_video_pool_set_allocation_notify (GstBufferPool *pool,
                      gpointer usr_data, GstQtiVideoPoolAllocNotify notify);

/**
 * gst_qti_video_pool_remove_allocation_notify:
 * @pool: a #GstQtiVideoBufferPool
 * @usr_data: a pointer to a parameter of callback
 * @notify: a #GstQtiVideoPoolAllocNotify for buffer allocation
 *
 * remove the callback to do not be notified about the allocation,
 * please ensure the usr_data is the same as for registering.
 */
void              gst_qti_video_pool_remove_allocation_notify (GstBufferPool *pool,
                      gpointer usr_data, GstQtiVideoPoolAllocNotify notify);

#ifdef G_DEFINE_AUTOPTR_CLEANUP_FUNC
G_DEFINE_AUTOPTR_CLEANUP_FUNC(GstQtiVideoBufferPool, gst_object_unref)
#endif

G_END_DECLS

#endif /* __GST_QTI_VIDEO_POOL_H__ */
